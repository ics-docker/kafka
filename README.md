# Kafka docker image

Docker image to run [Kafka](https://kafka.apache.org/).

## Usage

This image requires [ZooKeeper](https://zookeeper.apache.org/). An official image for ZooKeeper can be found at [Docker Hub](https://hub.docker.com/_/zookeeper).

    $ docker run registry.esss.lu.se/ics-docker/kafka:latest

would run a Kafka instance expecting ZooKeeper at localhost:2181

To override the default settings, mount a configuration file at /opt/kafka/config/server.properties.

    $ docker run -v $(pwd)/test/server.properties:/opt/kafka/config/server.properties:ro registry.esss.lu.se/ics-docker/kafka:latest
