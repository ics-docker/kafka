FROM openjdk:8u232-jre-slim

ENV KAFKA_VERSION=3.0.0 \
    KAFKA_HOME=/opt/kafka \
    KAFKA_LOGS=/tmp/kafka-logs

RUN useradd -m -s /bin/bash kafka

ADD --chown=kafka:kafka https://downloads.apache.org/kafka/${KAFKA_VERSION}/kafka_2.13-${KAFKA_VERSION}.tgz ${KAFKA_HOME}/

RUN tar -xvf ${KAFKA_HOME}/kafka_2.13-${KAFKA_VERSION}.tgz --strip 1 -C ${KAFKA_HOME} && \
    rm -f ${KAFKA_HOME}/kafka_2.13-${KAFKA_VERSION}.tgz

USER kafka

CMD ${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_HOME}/config/server.properties
